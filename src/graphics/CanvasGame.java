package graphics;

import java.awt.*;

import javax.swing.*;

import game.Board;

@SuppressWarnings("serial")
public class CanvasGame extends Canvas{
	public static final int squareSize = 80;
	public static final int margin = 3;
	
	private int [][] shotMatrix;
	
	public CanvasGame(Board board) {
		shotMatrix = new int [board.getLines()][board.getColumns()]; 
		
		for (int i = 0; i < board.getLines(); i++) {
			for (int j = 0; j < board.getColumns(); j++) {
				shotMatrix[i][j] = 0;
			}
		}
	}
	
	public void paint(Graphics painter, Board board) {
		ImageIcon boardIconBackground = new ImageIcon("images/ocean.jpg");
		ImageIcon shotIcon = new ImageIcon("images/water_shot.png");
		ImageIcon foundIcon = new ImageIcon("images/found.png");
		
		final Image boardBackground = boardIconBackground.getImage();
		final Image shot = shotIcon.getImage();
		final Image found = foundIcon.getImage();
		
		for (int i = 0; i < board.getLines(); i++) {
			for (int j = 0; j < board.getColumns(); j++) {
				painter.drawImage(boardBackground, (squareSize * j) + (margin * j) + 8, (squareSize * i) + (margin * i) + 8, squareSize, squareSize, null);
				
				if (shotMatrix[i][j] == 1) {
					painter.drawImage(found, (squareSize * j) + (margin * j) + 8, (squareSize * i) + (margin * i) + 8, squareSize, squareSize, null);
				}
				
				if (shotMatrix[i][j] == 2) {
					painter.drawImage(shot, (squareSize * j) + (margin * j) + 8, (squareSize * i) + (margin * i) + 8, squareSize, squareSize, null);
				}
			}
		}
		
		for (int i = 0; i < board.getBoats().size(); i++) {
			for (int j = 0; j < board.getBoats(i).size(); j++) {
				if (board.getBoats(i).get(j).getState()) {
					ImageIcon boatIcon = new ImageIcon("images/boat.jpg");
					final Image boat = boatIcon.getImage();
					
					int [] location = board.getBoats(i).get(j).getLocation();
					
					for(int k = 0; k < location.length; k+=2) {
						painter.drawImage(boat, (squareSize * location[k+1]) + (margin * location[k+1]) + 8, (squareSize * location[k]) + (margin * location[k]) + 8, squareSize, squareSize, null);
						shotMatrix[location[k]][location[k+1]] = 0;
					}
				}
			}
		}
	}
		
	public void setShot(int x, int y) {
		shotMatrix[x][y] = 1;
	}
	
	public void setWaterShot(int x, int y) {
		shotMatrix[x][y] = 2;
	}

}
