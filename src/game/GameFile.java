package game;

import java.io.IOException;
import java.io.FileReader;
import java.util.Scanner;

public class GameFile {
	private FileReader file;
	private Scanner readFile;
	public int line, column;
	public int [][] board;
	public int [] boats = new int [5];
	
	public GameFile(String path) {
		try {
			setFile(path);
			setReadFile(file);
			
			
			readFile.nextLine();
			column = readFile.nextInt();
			line = readFile.nextInt();
			
			readFile.nextLine();
			readFile.nextLine();
			readFile.nextLine();
			
			board = new int [line][column];
			for (int i = 0; i < line; i++) {
				char [] aux = readFile.next().toCharArray();

				for (int j = 0; j < column; j++) {
					board[i][j] = aux[j] - 48;
				}
			}

			readFile.nextLine();
			readFile.nextLine();
			readFile.nextLine();
			for (int i = 0; i < 5; i++) {
				readFile.next();
				boats[i] = readFile.nextInt();
			}

			readFile.close();
			file.close();
						
		} 
		catch (IOException e) {
			System.out.println("Impossible open the game file!");
		}
		
	}
	
	
	public void setFile(String path) {
		try {
			file = new FileReader(path);
		} catch (IOException e) {
			System.out.println("Impossible open the game file!");
		}
	}
	
	public void setReadFile(FileReader file) {
		readFile = new Scanner(file);
	}

		
	public FileReader getFile() {
		return file;
	}

	public Scanner getGetFile() {
		return readFile;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public int[][] getBoard() {
		return board;
	}

	public int[] getBoats() {
		return boats;
	}
	
}

